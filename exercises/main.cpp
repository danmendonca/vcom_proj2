#include "Image.hpp"
#include "Video.hpp"
#include <thread>
#include <iostream>
#include <algorithm>
#include <functional>
#include <iostream>


#include <vector>
#include <numeric>
#include <string>
#include "Image.hpp"

#include <opencv2\nonfree\features2d.hpp>

using namespace std;

int main(int argc, char** argv)
{
	cv::Mat mat = cv::imread("./resources/india.jpg", CV_LOAD_IMAGE_GRAYSCALE);
	cv::Mat mat2 = cv::imread("./resources/left.png", CV_LOAD_IMAGE_GRAYSCALE);


	cv::Ptr<cv::Feature2D> f2d = cv::SIFT::create("");
	
	cv::namedWindow("window", CV_WINDOW_NORMAL);
	cv::imshow("window", mat);
	
    
    Image i("./resources/feup1.png");
    
    cv::Mat output;
    
    cv::cornerHarris(i.grayScaleImage().matrix, output, 2,3,0.04);
    
    cv::normalize(output, output,0,255,cv::NORM_MINMAX,CV_32FC1, cv::Mat());
    cv::convertScaleAbs(output, output);
    cv::threshold(output, output, 130, 255, cv::THRESH_BINARY);
    
    i.grayScaleImage().display("Original");
    Image outputImage;
    outputImage.matrix = output;
    outputImage.type = GrayScale;
    outputImage.display("Harris");
    
    
    cv::waitKey();

}
