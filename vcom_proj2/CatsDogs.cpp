#include "CatsDogs.h"



CatsDogs::CatsDogs(int max)
{
	this->MAX_CATS_DOGS_NUMBER = max;
	cats = std::vector<std::string>();
	dogs = std::vector<std::string>();
}

CatsDogs::CatsDogs() {
	MAX_CATS_DOGS_NUMBER = 6000;
}


CatsDogs::~CatsDogs()
{
}


bool CatsDogs::addAnimal(std::string path, char * animal) {
	bool added = false;

	if (animal[0] == 'c' || animal[0] == 'C') {
		if (cats.size() < MAX_CATS_DOGS_NUMBER) {
			std::string cat(animal);
			std::string fullpath = path + "/" + cat;
			cats.push_back(fullpath);
			added = true;
		}

	}
	else {
		if (animal[0] == 'd' || animal[0] == 'D')
			if (dogs.size() < MAX_CATS_DOGS_NUMBER) {
				std::string dog(animal);
				std::string fullpath = path + "/" + dog;
				dogs.push_back(fullpath);
				added = true;
			}
	}
	return added;
}

size_t CatsDogs::nCats() {
	return cats.size();
}

size_t CatsDogs::nDogs() {
	return dogs.size();
}
