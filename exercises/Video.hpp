#ifndef Video_hpp
#define Video_hpp

#include <stdio.h>
#include <string>
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <functional>
#include <algorithm>

//right arrow ASCII = 2555904
//up arrow ASCII = 2490368
//down arrow ASCII = 2621440



using namespace std;



//typedef cv::Mat(*FrameProcessor)(cv::Mat);



class Displayer {



    function<vector<cv::Mat>(cv::Mat)> processor;
	std::string displayerName;


public:
    Displayer(std::string name,function<vector<cv::Mat>(cv::Mat)> processor) {

		this->displayerName = name;
		this->processor = processor;

	}


	void display(cv::Mat frame) {



        

		if (processor) {
            
            
            
            vector<cv::Mat> processorOutput = processor(frame);
            
            
            int counter = 1;
            for_each(processorOutput.begin(), processorOutput.end(), [this,&counter](cv::Mat output){
            
                
                imshow(displayerName+"_"+to_string(counter++), output);
            
            
            });

			//frame = processor(frame);
		}


	}


};


class Video;

class KeyAction {






public:
	int keyValue;
	

    function<void(Video *v, cv::Mat frame)> action;
	KeyAction(int keyValue, function<void(Video *v, cv::Mat frame)> action) :keyValue(keyValue), action(action) {

	}


};


class Video {

	cv::VideoCapture cap;

	int captureFrameKeyCode;


	enum VIDEO_MODE
	{
		PLAY,
		FRAME
	};

	std::vector<Displayer> displayers;
	std::vector<KeyAction> keyActions;






public:

#ifdef _WIN32
	static const int ASCII_ARROW_RIGHT = 2555904;
	static const int ASCII_ARROW_DOWN = 2621440;
	static const int ASCII_ARROW_UP = 2490368;
#else
    static const int ASCII_ARROW_RIGHT =63235;
    static const int ASCII_ARROW_DOWN =63233;
    static const int ASCII_ARROW_UP =63232;
#endif
    //right arrow ASCII = 255904
	//up arrow ASCII = 2490368
	//down arrow ASCII = 2621440

	void fastforwardSec(int s);
	void fastforwardMSec(int ms);

	cv::Mat capturedFrame;

    std::function<std::vector<cv::Mat>(cv::Mat)> processor;

	Video(int camera);
	Video(string file);


    void addDisplayer(string name, function<std::vector<cv::Mat>(cv::Mat)> processor);

	void addKeyAction(int keyCode, function<void(Video *v, cv::Mat frame)> action);

	void display();

	void setCaptureFrameKey(int keyCode, function<void()> callback);

	void saveFrame(cv::Mat frame);

};

#endif /* Video_hpp */
