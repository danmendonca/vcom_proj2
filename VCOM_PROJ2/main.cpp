//#################Common######################
#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

//################OPENCV 2.4.12##################
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/ml/ml.hpp>

//################SO Dependent################
#ifdef __APPLE__
#include <dirent.h>
#else
#include "dirent.h"
#include <direct.h> // _mkdir
#include <Windows.h>
#endif

//##############Custom#######################
#include "CatsDogs.h"
/**/

#define dictionaryFileName_NoExtension "dictionary"
#define descriptorsFilePath_NoExtension_ "svm"

using namespace cv;
using std::endl;
using std::cout;
using std::vector;
using std::string;
using std::cin;
using std::for_each;


std::vector<CatsDogs> catsDogsFiles(const string &path, const std::size_t split);
bool createOutputDir(string oPath);
inline bool exists(const std::string& name);
Mat dictionary(std::string fileName);
cv::Mat BowDescriptiorForImage(cv::Mat dictionary, cv::Mat img);
cv::Mat getVocabulary(std::vector<std::string> imgsPath);
int trainSVM(std::vector<std::string> positive, std::vector<std::string> negative, cv::Mat dictionary, string svmFileName, int penalty);
void writeToYAMLFile(std::string filename, std::string key, Mat mat, bool append = false);
Mat readFromYAMLFile(std::string filename, std::string key);


/*******************************************************
 MAIN
 *******************************************************/
int main(int argc, const char * argv[]) {
	string outputPath, vocabularyImgPath, imagesDirectory;
	int nrTestSets, dicNrWords, nrVocImgs, penalty;
	int testSetNr;
	string svm_file_name, dictionary_file_name, results;
	if (argc < 9) {
		cout << "Missing parameters. Usage vcom_proj2 [vocabularyImgPath] [nrVocabularyImgs] [dictionaryNrWords] [trainImagePath] [nrTestGroups] [setToTest] [penalty] [outputPath]" << endl;
		waitKey();
		exit(-1);
	}

	string error_name;
	vocabularyImgPath = string(argv[1]);
	outputPath = string(argv[8]);
	imagesDirectory = string(argv[4]);

	try {
		error_name = "nrVocabularyImages";
		nrVocImgs = atoi(string(argv[2]).c_str());
		error_name = "dictionaryNrImg";
		dicNrWords = atoi(string(argv[3]).c_str());
		error_name = "nrTestGroups";
		nrTestSets = atoi(string(argv[5]).c_str());
		error_name = "testSetNr";
		testSetNr = atoi(string(argv[6]).c_str());
		error_name = "penalty";
		penalty = atoi(string(argv[7]).c_str());
	}
	catch (Exception *e) {
		cout << error_name << ":" << endl;
		cout << e->msg << endl;
		waitKey();
		exit(-1);
	}

	char buff[70];
	if (outputPath.at(outputPath.size() - 2) != '/')
		outputPath += "/";

	sprintf(buff, "%s%s_%i_%i", outputPath.c_str(), dictionaryFileName_NoExtension, nrVocImgs, dicNrWords);
	dictionary_file_name = string(buff);
	sprintf(buff, "%s__%s_%d_%d_%d", dictionary_file_name.c_str(), descriptorsFilePath_NoExtension_, nrTestSets, testSetNr, penalty);
	svm_file_name = string(buff);
	sprintf(buff, "%s__results.csv", svm_file_name.c_str());
	results = string(buff);
	svm_file_name += ".xml";
	dictionary_file_name += ".yaml";

	Mat dictionary;
	CvSVM svm;
	CatsDogs trainSet;
	bool sampling = false;
	if (argc == 10) {
		string lastArg(argv[9]);
		if (strcmp(lastArg.c_str(), "-f") == 0) {
			sampling = true;
		}
	}

	if (!exists(dictionary_file_name)) {
		createOutputDir(outputPath);
		std::cout << "###########Creating dictionary###########" << std::endl;
		vector<std::string> files;
		for (int i = 1; i < nrVocImgs; i++) {
			char buffer[50];
			sprintf(buffer, "%s/%d.jpg", vocabularyImgPath.c_str(), i);
			files.push_back(std::string(buffer));
		}
		auto featuresUnclustered = getVocabulary(vector<std::string>(files));//CHANGE THIS
				//the number of bags(words)
		int dictionarySize = dicNrWords;
		//define Term Criteria
		TermCriteria tc(CV_TERMCRIT_ITER, 100, 0.001);
		//retries number
		int retries = 1;
		//necessary flags
		int flags = KMEANS_PP_CENTERS;
		//Create the BoW (or BoF) trainer
		BOWKMeansTrainer bowTrainer(dictionarySize, tc, retries, flags);
		//cluster the feature vectors
		dictionary = bowTrainer.cluster(featuresUnclustered);
		writeToYAMLFile(dictionary_file_name, "vocabulary", dictionary);
	}

	dictionary = readFromYAMLFile(dictionary_file_name, "vocabulary");
	auto catsDogsSets = catsDogsFiles(imagesDirectory.c_str(), nrTestSets);
	trainSet = catsDogsSets.at(testSetNr);
	
	if (!exists(svm_file_name))
	{
		std::cout << "SVM Classifier not found!" << std::endl;
		std::cout << "###########Training SVM###########" << std::endl;
		vector<string> cats = trainSet.cats, dogs = trainSet.dogs;
		cout << "Starting training for " << cats.size() + dogs.size() << " images" << endl;
		trainSVM(cats, dogs, dictionary, string(svm_file_name), penalty);
	}

	svm.load(svm_file_name.c_str());
	int correct = 0;
	int total = 0;
	int current_group = 0;
	int imageNr = 1;
	int nrImages = 0;
	for (CatsDogs c_d : catsDogsSets) {
		if (c_d == trainSet)
			continue;
		nrImages += c_d.cats.size() + c_d.dogs.size();
	}

	if (!exists(results)) {
		for_each(catsDogsSets.begin(), catsDogsSets.end(), [&](CatsDogs elem)
		{
			if (elem == trainSet) {
				current_group++;
				return;
			}
			
			cout << "Starting new group: " << current_group << " of " << nrTestSets-1 << endl;
			for_each(elem.cats.begin(), elem.cats.end(), [&](string e)
			{
				imageNr++;
				if (sampling)
				{
					int nr = rand() % 100;
					if (nr > 10)return;
				}
				total++;
				Mat img = imread(e);
				auto descriptor = BowDescriptiorForImage(dictionary, img);
				auto value = svm.predict(descriptor);
				if (value > 0)
				{
					correct++;
					cout << "Group: " << current_group << "  Image " << imageNr << " of " << nrImages << " Current ratio = " << float(correct / float(total)) << endl;
				}
			});
			for_each(elem.dogs.begin(), elem.dogs.end(), [&](string e)
			{
				imageNr++;
				if (sampling)
				{
					int nr = rand() % 100;
					if (nr > 10)return;
				}
				total++;
				Mat img = imread(e);
				auto descriptor = BowDescriptiorForImage(dictionary, img);
				auto value = svm.predict(descriptor);
				if (value < 0)
				{
					correct++;
					cout << "Group: " << current_group << "  Image " << imageNr << " of " <<nrImages <<" Current ratio = " << float(correct / float(total)) << endl;
				}
			});
			current_group++;
		});
		auto ratio = correct / float(total);
		cout << "Ratio = " << ratio << endl;
		std::ofstream resultsFile;
		resultsFile.open(results);
		resultsFile << "nr_voc_img," << "nr_words," << "nr_groups," << "svm_group_nr," << "penalty," << "success_rate," << endl;
		resultsFile << nrVocImgs << "," << dicNrWords << "," << nrTestSets << "," << testSetNr << "," << penalty << "," << ratio << "," << endl;
		resultsFile.close();
	}
	//If by some reason you want to test a specific image from command line, uncomment the following code.
	/*
	while (1) 
	{
		string buff = "";
		cout << "Please enter a path to an image:" << endl;
		getline(cin, buff);
		Mat img = imread(buff);
		if (img.rows == 0 || img.cols == 0) 
		{
			cout << "Invalid image. Use another one." << endl;
			continue;
		}
		
		auto descriptor = BowDescriptiorForImage(dictionary, img);
		auto value = svm.predict(descriptor);
		if (value > 0)	cout << "It's a Cat" << endl;
		 else	cout << "It's a Dog" << endl;
	}
	*/
}


std::vector<CatsDogs> catsDogsFiles(const string &path, const std::size_t split)
{
	DIR *dir;
	struct dirent *ent;
	std::size_t index = 0;
	std::vector<CatsDogs> sets = std::vector<CatsDogs>(split);
	for (std::size_t i = 0; i < split; i++)
		sets.at(i) = CatsDogs(6000);
	if ((dir = opendir(path.c_str())) != NULL)
	{
		/* print all the files and directories within directory */
		while ((ent = readdir(dir)) != NULL)
		{
			sets.at(index).addAnimal(path, ent->d_name);
			index++;
			index = index % split;
		}
		closedir(dir);
	}
	else
	{
		/* could not open directory */
		perror("");
	}
	return sets;
}
bool createOutputDir(string oPath)
{
	bool created = false;
	int k;
#ifdef __APPLE__
	k = mkdir(oPath.c_str(), 0755);
#else WINDOWSFOLDER
	k = _mkdir(oPath.c_str());
#endif
	created = (k == 0) ? true : false;
	cout << "Output Directory creation: " << created << endl;
	return created;
}
inline bool exists(const std::string& name)
{
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}
Mat dictionary(std::string fileName)
{
	cv::Mat dictionary;
	cv::FileStorage fs(fileName, cv::FileStorage::READ);
	fs["vocabulary"] >> dictionary;
	fs.release();
	return dictionary;
};
cv::Mat BowDescriptiorForImage(cv::Mat dictionary, cv::Mat img)
{
	//create a nearest neighbor matcher
	Ptr<DescriptorMatcher> matcher(new FlannBasedMatcher);
	//create Sift feature point extracter
	Ptr<FeatureDetector> detector(new SiftFeatureDetector());
	//create Sift descriptor extractor
	Ptr<DescriptorExtractor> extractor(new SiftDescriptorExtractor);
	//create BoF (or BoW) descriptor extractor
	BOWImgDescriptorExtractor bowDE(extractor, matcher);
	//Set the dictionary with the vocabulary we created in the first step
	bowDE.setVocabulary(dictionary);
	//To store the keypoints that will be extracted by SIFT
	vector<KeyPoint> keypoints;
	//Detect SIFT keypoints (or feature points)
	detector->detect(img, keypoints);
	//To store the BoW (or BoF) representation of the image
	Mat bowDescriptor;
	//extract BoW (or BoF) descriptor from given image
	bowDE.compute(img, keypoints, bowDescriptor);
	return bowDescriptor;
};
cv::Mat getVocabulary(std::vector<std::string> imgsPath)
{
	cv::Mat input;
	vector<KeyPoint> keypoints;
	SiftDescriptorExtractor detector;
	Mat descriptor;
	//To store all the descriptors that are extracted from all the images.
	Mat featuresUnclustered;
	std::for_each(imgsPath.begin(), imgsPath.end(), [&](std::string elem)
	{
		std::cout << elem << std::endl;
		input = imread(elem, CV_LOAD_IMAGE_GRAYSCALE); //Load as grayscale
													   //detect feature points
		detector.detect(input, keypoints);
		//compute the descriptors for each keypoint
		detector.compute(input, keypoints, descriptor);
		//put the all feature descriptors in a single Mat object
		featuresUnclustered.push_back(descriptor);
	});
	return featuresUnclustered;
};
int trainSVM(std::vector<std::string> positive, std::vector<std::string> negative, cv::Mat dictionary, string svmFileName, int penalty)
{
	// create training data
	Mat train;
	Mat response;
	Mat img;
	//train with positive examples
	std::for_each(positive.begin(), positive.end(), [&](std::string elem)
	{
		img = imread(elem, CV_LOAD_IMAGE_GRAYSCALE);
		train.push_back(BowDescriptiorForImage(dictionary, img));
		response.push_back(1);
	});
	//train with negative examples
	std::for_each(negative.begin(), negative.end(), [&](std::string elem)
	{
		img = imread(elem, CV_LOAD_IMAGE_GRAYSCALE);
		train.push_back(BowDescriptiorForImage(dictionary, img));
		response.push_back(-1);
	});
	// svm parameters
	CvTermCriteria criteria = cvTermCriteria(CV_TERMCRIT_EPS, 1000, FLT_EPSILON);
	CvSVMParams svm_param = CvSVMParams(CvSVM::C_SVC, CvSVM::RBF, 2.0, 8.0, 1.0, penalty, 0.5, 0.1, NULL, criteria);
	// train svm
	cv::SVM svm;
	svm.train(train, response, cv::Mat(), cv::Mat(), svm_param);
	//store results in a file
	svm.save(svmFileName.c_str());
	return 0;
}
void writeToYAMLFile(std::string filename, std::string key, Mat mat, bool append)
{
	int flags = FileStorage::WRITE;
	if (append) flags = FileStorage::APPEND;
	FileStorage fs(filename, flags);
	fs << key << mat;
	fs.release();
}
Mat readFromYAMLFile(std::string filename, std::string key)
{
	Mat theMat;
	FileStorage fs(filename, FileStorage::READ);
	fs[key] >> theMat;
	fs.release();
	return theMat;
}