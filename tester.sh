#!/bin/bash
while read argLine
do
echo $($2 $argLine/$(hexdump -n 16 -v -e '/1 "%02X"' /dev/urandom))
done < "$1"