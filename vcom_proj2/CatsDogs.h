#pragma once

#include <stdlib.h>
#include <vector>
#include <iostream>


class CatsDogs
{
public:
	CatsDogs(int max);
	CatsDogs();
	~CatsDogs();
	
	std::size_t MAX_CATS_DOGS_NUMBER;
	std::vector<std::string> cats;
	std::vector<std::string> dogs;

	std::size_t nCats();
	std::size_t nDogs();

	bool addAnimal(std::string, char *);
    
    bool operator==(const CatsDogs& rhs) const {
        
        return rhs.cats == this->cats && rhs.dogs == this->dogs ;
        
    };

};

