@ECHO OFF
FOR %%n in (1,2,3,4,0) DO (
	ECHO %TIME%
	FOR %%i in (1000) DO (
		FOR %%j in (100) DO (
			FOR %%k in (5) DO (
				ECHO.
				ECHO Dictionary Images- %%i
				ECHO Dictionary Words- %%j
				ECHO Trainning Sets Number- %%k
				ECHO TrainningSet Number- %%n
				ECHO Penalty value- 10
				START /WAIT vcom_sift_chi2.exe ./resources/vocabulary %%i %%j ./resources/cats_dogs %%k %%n 10 ./resources/surf_rbf
			)
		)
	)
)