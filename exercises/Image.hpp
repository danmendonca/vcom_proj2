#ifndef Image_hpp
#define Image_hpp

#include <stdio.h>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <functional>


cv::Mat setRoiForLines(cv::Mat &image, cv::Vec4i const &line1, cv::Vec4i const &line2, int tau);

struct PerspectiveConf {

	std::vector<cv::Point2f> original;
	std::vector<cv::Point2f> dest;
};


using namespace std;
enum ImageChannel { RED, GREEN, BLUE, HUE, SATURATION, VALUE };


enum ImageType {


	GrayScale,
	RGB,
	HSV

};


float slope(cv::Vec4i line);

float inv_slope(cv::Vec4i line);

vector<cv::Point2f> extendLine(int minY,int maxY , cv::Point2f pt1,cv::Point2f pt2);

cv::Vec4i extendLine(int minY,int maxY , cv::Vec4i line);

void sortCorners(std::vector<cv::Point2f>& corners, cv::Point2f center);

cv::Point2f computeIntersect(cv::Vec4i a, cv::Vec4i b);
double median (cv::Mat image);
cv::Mat auto_canny(cv::Mat const & mat,float sigma = 0.33);


vector<cv::Point2f> transformLine(cv::Mat transform, vector<cv::Point2f>line);
vector<cv::Point2f> transformLine(std::vector<cv::Point2f> original,std::vector<cv::Point2f> dest,vector<cv::Point2f>line);

cv::Mat customEdgeDetectorImage(const cv::Mat &mat,int tau,float cleanPortion = 0.5,ImageType type = RGB);
cv::Mat toGrayscale(const cv::Mat& mat,ImageType type);


void thinning(cv::Mat & input);

bool isDummyPerspectiveConf(PerspectiveConf conf);
class Image
{
public:

	const static int MIN_THRESHOLD = 10;
	const static int MAX_THRESHOLD = 500;
	ImageType type;
	cv::Mat matrix;

	Image();

	Image(int rows, int cols, int value);
	Image(std::string path);

	cv::Mat getImage();
	Image grayScaleImage();
	Image saltAndPepperNoiseImage(int noisePercentage = 10);

	Image channelImage(ImageChannel channel);

	Image imageByOffsettingChannel(ImageChannel channel, int val);

	Image hsvImage();
	Image rgbImage();

	Image histogramImage();
	Image equalizedImage();
	Image claheImage(double clipLimit, cv::Size tileGridSize);

	Image meanFilter(cv::Size size);
	Image medianFilter(int size);
	Image gaussianFilter(cv::Size size);

	Image cannyImage(double t1, double t2, bool overlap);

	Image linesImage(Image cannyRawOutputImage, vector<cv::Vec4i> lines = vector<cv::Vec4i>());
	Image linesImage_points(Image cannyRawOutputImage, vector<cv::Point2f> lines);

	Image linesImageContinuos(Image greyscaleLined);

	Image customEdgeDetectorImage(int tau, bool clean = false);

	Image equalizeUsingTransformer(std::function<cv::Mat(cv::Mat frame)>);

	Image laplacianImage();



	void resetImage();

	Image floodFillImage(int lowerThreshold = MIN_THRESHOLD, int upperThreshold = MAX_THRESHOLD);

	Image cannyImageAuto(int &lowerThreshold, int &upperThreshold, int lowerMean = 3, int upperMean = 12);

	Image cannyAutoThreshold(int &lowerThreshold, int &upperThreshold, int lowerMean = 3, int upperMean = 12);

	Image distortImage(PerspectiveConf conf);

	PerspectiveConf distortParameters(int lowerThreshold = MIN_THRESHOLD, int upperThreshold = MAX_THRESHOLD, bool isCanny = false);

	Image customCannyProcess(int &lowerTreshold, int &upperTreshold, cv::Size size = {5,5});

	Image getBlurredCustomEdgeImage(cv::Size size = {5,5});

	vector<cv::Vec4i> lines(Image canny);

	Image(cv::Mat mat, ImageType type);

	std::vector<cv::Mat> channels();

	std::vector<Image> channelImages();

	int channelIndex(ImageChannel channel);
	void display(std::string name);
	void save(std::string path);

private:




};



#endif /* Image_hpp */
